const app = require("./app/server");
const port = process.env.PORT || 3000;

console.log(`Starting app on port ${port}`);
app.listen(port);
